
  <body>

    <div class="navbar  navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="index.php"><?php print $siteName;?></a>

          <div class="nav-collapse collapse">
            
            <ul class="nav">
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="list.php">List Example</a></li>
              <li><a href="grid.php">Grid Example</a></li>
            </ul>
            <?php
            $searchForm = '<form class="navbar-search pull-right" action=""><input type="text" class="search-query span4" placeholder="Search"></form>';
            if ($pageName != "home"){
              print $searchForm;
            }
            ?>
            <p class="navbar-text pull-right">
              Logged in as <a href="#" class="navbar-link"><?php print $username;?></a>
            </p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>