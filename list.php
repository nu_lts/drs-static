<?php
  $pageName = "List-View";
  $siteName = "DRS digital repository service";
  $username = "sbasett";
  include('inc/head.php');
  include('inc/navbar.php');
?>
    <div class="container-fluid">
        <?php include('inc/sidebar.php');?>
        <div class="span9">
          <?php
            $listItem = '<li class="media">
              <a class="pull-left" href="#">
                <img class="media-object" data-src="holder.js/150x150">
              </a>
              <div class="media-body">
                <h4 class="media-heading">Media heading</h4>
                <p>Retro aute bicycle rights consectetur, gluten-free proident cred american apparel. Cardigan duis laboris dolore jean shorts deserunt keytar, gastropub master cleanse aesthetic odd future. Ad adipisicing four loko sed, flexitarian viral proident eiusmod non mustache aliqua godard 3 wolf moon. Locavore vice occaecat four loko 3 wolf moon tumblr. Synth williamsburg gentrify trust fund. Trust fund id est typewriter, in pour-over banh mi terry richardson hella laboris cliche cillum locavore. Kale chips placeat incididunt photo booth helvetica semiotics.</p>
              </div>
              </li>';
            $listItems = "";
            for ($i = 1; $i <=50; $i++){
                $listItems = $listItems . $listItem;
              }
            print '<ul class="media-list">' . $listItems . '</ul>'
          ?>
        </div><!--/span-->

      </div><!--/row-->
<?php include('inc/footer.php');?>